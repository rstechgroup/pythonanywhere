PythonAnywhere
===============

*A PythonAnywhere API client.*

Usage
-----

    from paw_api.client import PythonAnywhere

    # If running on a PythonAnywhere terminal, your token and username will
    # be discovered automatically. Be sure to setup your API_TOKEN first.

    # For information on setting up your API_TOKEN visit
    # http://help.pythonanywhere.com/pages/API
    API_TOKEN = "test-token"

    # Your PythonAnywhere Username
    USER = "test-user"

    client = PythonAnywhere(api_key=API_TOKEN, user=USER)

Endpoints
---------

CPU:

    # View the CPU usage
    response = client.cpu()

Consoles:

    # Create a new console object
    response = client.consoles.create(data={
                'executable': 'bash',
                'arguments': './run_script.py',
                'working_directory': '/user/home/directory'
            })

    # List all your consoles
    response = client.consoles()

    # View consoles shared with you
    response = client.consoles.shared_with_you()

    # Get console by id
    response = client.consoles(console_id=123456)

    # Kill a console by id
    response = client.consoles.delete(console_id=123456)

    # Send input to console
    response = client.consoles.send_input.send(
                    console_id=123456,
                    data={'input':'pwd\nls\n'}
                )

    # Get latest output from console (Approximately 500 characters)
    response = client.consoles.get_latest_output(console_id=123456)

Files:

    # Download file from the path
    response = client.files.path(path='/path/to/file')

    # Upload file to the path
    with open('config.ini', 'rb') as file:
        response = client.files.path.send(
                    path='/path/to/file',
                    files={'content': file}
                )

    # Delete file from the path
    response = client.files.path.delete(path='/path/to/file')

Sharing Files:

    # Start sharing a file
    response = client.files.sharing.create(data={"path": "/path/to/file"})

    # Get sharing status by path
    response = client.files.sharing(params={"path": "/path/to/file"})

    # Stop sharing a file
    response = client.files.sharing.delete(params={"path": "/path/to/file"})

File Tree:

    # List contents of a directory and subdirectories
    response = client.files.tree(params={"path": "/path/to/file"})

Tasks:

    # List all scheduled tasks
    response = client.schedule()

    # Create a new scheduled task
    response = client.schedule.create(data={
                "command": "python37 script.py",
                "enabled": True,
                "interval": "daily",
                "hour": "14",
                "minute": "53"
            })

    # Get scheduled task details by id
    response = client.schedule(task_id=123456)

    # Modify all configs of a task
    response = client.schedule.update(task_id=123456, data={
                "command": "python27 script.py",
                "enabled": True,
                "interval": "daily",
                "hour": "14",
                "minute": "53"
            })

    # Modify specific configs of a task
    response = client.schedule.update_specific(task_id=123456, data={
                "enabled": True,
                "hour": "14",
                "minute": "00"
            })

    # Delete scheduled task by id
    response = client.schedule.delete(task_id=123456)

Web Apps:

    # List all webapps
    response = client.webapps()

    # Create a new webapp
    response = client.webapps.create(data={
            "domain_name": "username.pythonanywhere.com",
            "python_version": "python27",
        })

    # Get webapp by domain name
    response = client.webapps(domain_name="username.pythonanywhere.com")

    # Modify all configs of a webapp. Follow with reloading webapp.
    # Arguments (python_version, source_directory, virtualenv_path, force_https)
    response = client.webapps.update(
        domain_name="username.pythonanywhere.com", data={
            "python_version": "3.6",
            "source_directory": "/path/to/source",
            "virtualenv_path": "/path/to/virtualenv",
            "force_https": True
        }
    )

    # Modify specific config of a webapp. Follow with reloading webapp.
    # Arguments (python_version, source_directory, virtualenv_path, force_https)
    response = client.webapps.update_specific(
        domain_name="username.pythonanywhere.com", data={
            "python_version": "3.6",
            "virtualenv_path": "/path/to/virtualenv",
        }
    )

    # Delete webapp by domain name
    response = client.webapps.delete(domain_name="username.pythonanywhere.com")

    # Reload webapp
    response = client.webapps.reload.send(domain_name="username.pythonanywhere.com")

SSL:

    # Get SSL info
    response = client.webapps.ssl(domain_name="username.pythonanywhere.com")

Static Files:

    # List all static files mappings for a domain
    response = client.webapps.static_files(
        domain_name="username.pythonanywhere.com"
    )

    # Create a new static file mapping for a domain. Reload webapp required.
    response = client.webapps.static_files.create(
        domain_name="username.pythonanywhere.com", data={
            "url": "/static/", "path": "/path/to/static/dir"
        }
    )

    # Get static file mapping by id
    response = client.webapps.static_files(
        domain_name="username.pythonanywhere.com", static_id=123
    )

    # Modify all static file mapping by id. Reload webapp required.
    response = client.webapps.static_files.update(
        domain_name="username.pythonanywhere.com", static_id=123, data={
            "url": "/static/", "path": "/path/to/static/dir"
        }
    )

    # Modify specific static file mapping by id. Reload webapp required.
    response = client.webapps.static_files.update_specific(
        domain_name="username.pythonanywhere.com", static_id=123, data={
            "url": "/static/", "path": "/path/to/static/dir"
        }
    )

    # Delete a static file mapping by id. Reload webapp required.
    response = client.webapps.static_files.delete(
        domain_name="username.pythonanywhere.com", static_id=123
    )

Credit
------

This application uses Open Source components. You can find the license information below. We acknowledge and are grateful to these developers for their contributions to open source.

Copyright (c) 2019 [RS Technologies](https://srajkumar.pythonanywhere.com)
