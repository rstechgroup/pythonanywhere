from .paw_test_suite import PythonAnywhereTestCase
import responses


class TestFiles(PythonAnywhereTestCase):

    @responses.activate
    def test_download_file(self):
        url_path = "files/path/test/file/path/"
        responses.add(responses.GET, self.get_url(url_path))
        self.client.files.path(path="test/file/path")
        self.asserts(url_path)

    @responses.activate
    def test_upload_file(self):
        url_path = "files/path/test/file/path/"
        responses.add(responses.POST, self.get_url(url_path))
        self.client.files.path.create(path="test/file/path")
        self.asserts(url_path, "POST")

    @responses.activate
    def test_delete_file(self):
        url_path = "files/path/test/file/path/"
        responses.add(responses.DELETE, self.get_url(url_path))
        self.client.files.path.delete(path="test/file/path")
        self.asserts(url_path, "DELETE")
