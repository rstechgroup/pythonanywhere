from .paw_test_suite import PythonAnywhereTestCase
import responses


class TestCPU(PythonAnywhereTestCase):

    @responses.activate
    def test_cpu_usage(self):
        url_path = "cpu/"
        responses.add(responses.GET, self.get_url(url_path))
        self.client.cpu()
        self.asserts(url_path)
