from .paw_test_suite import PythonAnywhereTestCase
import responses


class TestFileTree(PythonAnywhereTestCase):

    @responses.activate
    def test_files_tree(self):
        url_path = "files/tree/?path=test%2Fpath"
        responses.add(responses.GET, self.get_url(url_path))
        self.client.files.tree(params={"path": "test/path"})
        self.asserts(url_path)
