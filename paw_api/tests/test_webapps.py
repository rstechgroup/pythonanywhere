from .paw_test_suite import PythonAnywhereTestCase
import responses


class TestWebapps(PythonAnywhereTestCase):

    @responses.activate
    def test_list_webapps(self):
        url_path = "webapps/"
        responses.add(responses.GET, self.get_url(url_path))
        self.client.webapps()
        self.asserts(url_path)

    @responses.activate
    def test_create_webapps(self):
        url_path = "webapps/"
        input_data = {
            "domain_name": "www.test.com",
            "python_version": "python27",
        }
        responses.add(responses.POST, self.get_url(url_path))
        self.client.webapps.create(data=input_data)
        expected_data = [
            "domain_name=www.test.com",
            "python_version=python27"
        ]
        self.asserts(url_path, "POST", expected_data)

    @responses.activate
    def test_get_webapps_by_domain_name(self):
        url_path = "webapps/test.com/"
        responses.add(responses.GET, self.get_url(url_path))
        self.client.webapps(domain_name="test.com")
        self.asserts(url_path)

    @responses.activate
    def test_update_all_webapps_fields_by_domain_name(self):
        url_path = "webapps/test.com/"
        input_data = {
            "python_version": "python37",
            "source_directory": "/path/to/source",
            "virtualenv_path": "/path/to/virtualenv",
            "force_https": True
        }
        responses.add(responses.PUT, self.get_url(url_path))
        self.client.webapps.update(domain_name="test.com", data=input_data)
        expected_data = [
            "python_version=python37",
            "source_directory=%2Fpath%2Fto%2Fsource",
            "virtualenv_path=%2Fpath%2Fto%2Fvirtualenv",
            "force_https=True"
        ]
        self.asserts(url_path, "PUT", expected_data)

    @responses.activate
    def test_update_specific_webapps_fields_by_domain_name(self):
        url_path = "webapps/test.com/"
        input_data = {
            "python_version": "python27",
            "force_https": False
        }
        responses.add(responses.PATCH, self.get_url(url_path))
        self.client.webapps.update_specific(domain_name="test.com", data=input_data)
        expected_data = [
            "python_version=python27",
            "force_https=False"
        ]
        self.asserts(url_path, "PATCH", expected_data)

    @responses.activate
    def test_delete_webapps_by_domain_name(self):
        url_path = "webapps/test.com/"
        responses.add(responses.DELETE, self.get_url(url_path))
        self.client.webapps.delete(domain_name="test.com")
        self.asserts(url_path, "DELETE")

    @responses.activate
    def test_reload_webapps_by_domain_name(self):
        url_path = "webapps/test.com/reload/"
        responses.add(responses.POST, self.get_url(url_path))
        self.client.webapps.reload.send(domain_name="test.com")
        self.asserts(url_path, "POST")
