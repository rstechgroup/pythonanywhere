from .paw_test_suite import PythonAnywhereTestCase
import responses


class TestStaticFiles(PythonAnywhereTestCase):

    @responses.activate
    def test_list_static_files_by_domain_name(self):
        url_path = "webapps/test.com/static_files/"
        responses.add(responses.GET, self.get_url(url_path))
        self.client.webapps.static_files(domain_name="test.com")
        self.asserts(url_path)

    @responses.activate
    def test_create_static_files_by_domain_name(self):
        url_path = "webapps/test.com/static_files/"
        input_data = {
            "url": "/static/",
            "path": "/test/path/"
        }
        responses.add(responses.POST, self.get_url(url_path))
        self.client.webapps.static_files.create(domain_name="test.com", data=input_data)
        expected_data = [
            "url=%2Fstatic%2F",
            "path=%2Ftest%2Fpath%2F"
        ]
        self.asserts(url_path, "POST", expected_data)

    @responses.activate
    def test_get_static_files_by_domain_name_and_static_id(self):
        url_path = "webapps/test.com/static_files/123/"
        responses.add(responses.GET, self.get_url(url_path))
        self.client.webapps.static_files(domain_name="test.com", static_id=123)
        self.asserts(url_path)

    @responses.activate
    def test_update_all_static_files_mapping_by_domain_name_and_static_id(self):
        url_path = "webapps/test.com/static_files/123/"
        input_data = {
            "url": "/static/",
            "path": "/test/path/"
        }
        responses.add(responses.PUT, self.get_url(url_path))
        self.client.webapps.static_files.update(domain_name="test.com", static_id=123, data=input_data)
        expected_data = [
            "url=%2Fstatic%2F",
            "path=%2Ftest%2Fpath%2F"
        ]
        self.asserts(url_path, "PUT", expected_data)

    @responses.activate
    def test_update_specific_static_files_mapping_by_domain_name_and_static_id(self):
        url_path = "webapps/test.com/static_files/123/"
        input_data = {
            "path": "/test/path/"
        }
        responses.add(responses.PATCH, self.get_url(url_path))
        self.client.webapps.static_files.update_specific(domain_name="test.com", static_id=123, data=input_data)
        expected_data = [
            "path=%2Ftest%2Fpath%2F"
        ]
        self.asserts(url_path, "PATCH", expected_data)

    @responses.activate
    def test_delete_static_files_by_domain_name_and_static_id(self):
        url_path = "webapps/test.com/static_files/123/"
        responses.add(responses.DELETE, self.get_url(url_path))
        self.client.webapps.static_files.delete(domain_name="test.com", static_id=123)
        self.asserts(url_path, "DELETE")
