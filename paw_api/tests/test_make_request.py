from paw_api.client import PythonAnywhereError
from .paw_test_suite import PythonAnywhereTestCase
import responses


class TestMakeRequest(PythonAnywhereTestCase):

    @responses.activate
    def test_make_request(self):
        responses.add(responses.GET, self.get_url("consoles/"))
        self.client.consoles()

        assert len(responses.calls) == 1
        call = responses.calls[0]
        headers = call.request.headers
        assert headers["Authorization"] == "Token api_key"
        assert call.response.ok is True

    @responses.activate
    def test_make_request_not_okay(self):
        responses.add(responses.GET, self.get_url("console/"), status=404)

        try:
            self.client.console()
        except PythonAnywhereError as e:
            assert len(responses.calls) == 1
            call = responses.calls[0]
            assert call.response.ok is False
