from .paw_test_suite import PythonAnywhereTestCase
import responses


class TestSharingFiles(PythonAnywhereTestCase):

    @responses.activate
    def test_share_file(self):
        url_path = "files/sharing/"
        responses.add(responses.POST, self.get_url(url_path))
        self.client.files.sharing.create(
            data={"path": "test/path"}
        )
        self.asserts(
            url_path,
            "POST",
            ["path=test%2Fpath"]
        )

    @responses.activate
    def test_file_sharing_status(self):
        url_path = "files/sharing/?path=test%2Fpath"
        responses.add(responses.GET, self.get_url(url_path))
        self.client.files.sharing(params={"path": "test/path"})
        self.asserts(url_path)

    @responses.activate
    def test_delete_sharing_a_file(self):
        url_path = "files/sharing/?path=test%2Fpath"
        responses.add(responses.DELETE, self.get_url(url_path))
        self.client.files.sharing.delete(params={"path": "test/path"})
        self.asserts(url_path, "DELETE")
