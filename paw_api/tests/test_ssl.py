from .paw_test_suite import PythonAnywhereTestCase
import responses


class TestSSL(PythonAnywhereTestCase):

    @responses.activate
    def test_ssl_info(self):
        url_path = "webapps/test.com/ssl/"
        responses.add(responses.GET, self.get_url(url_path))
        self.client.webapps.ssl(domain_name="test.com")
        self.asserts(url_path)
