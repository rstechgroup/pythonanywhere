from .paw_test_suite import PythonAnywhereTestCase
import responses


class TestConsoles(PythonAnywhereTestCase):

    @responses.activate
    def test_create_console(self):
        url_path = "consoles/"
        responses.add(responses.POST, self.get_url(url_path))
        self.client.consoles.create()
        self.asserts(url_path, "POST")

    @responses.activate
    def test_list_consoles(self):
        url_path = "consoles/"
        responses.add(responses.GET, self.get_url(url_path))
        self.client.consoles()
        self.asserts(url_path)

    @responses.activate
    def test_shared_consoles(self):
        url_path = "consoles/shared_with_you/"
        responses.add(responses.GET, self.get_url(url_path))
        self.client.consoles.shared_with_you()
        self.asserts(url_path)

    @responses.activate
    def test_get_console_by_id(self):
        url_path = "consoles/123/"
        responses.add(responses.GET, self.get_url(url_path))
        self.client.consoles(console_id=123)
        self.asserts(url_path)

    @responses.activate
    def test_delete_console_by_id(self):
        url_path = "consoles/123/"
        responses.add(responses.DELETE, self.get_url(url_path))
        self.client.consoles.delete(console_id=123)
        self.asserts(url_path, "DELETE")

    @responses.activate
    def test_send_input_to_console(self):
        url_path = "consoles/123/send_input/"
        responses.add(responses.POST, self.get_url(url_path))
        self.client.consoles.send_input.send(console_id=123)
        self.asserts(url_path, "POST")

    @responses.activate
    def test_get_console_output(self):
        url_path = "consoles/123/get_latest_output/"
        responses.add(responses.GET, self.get_url(url_path))
        self.client.consoles.get_latest_output(console_id=123)
        self.asserts(url_path)
