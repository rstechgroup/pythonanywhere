from paw_api.client import API_ENDPOINT, PythonAnywhere
from unittest import TestCase
import responses


class PythonAnywhereTestCase(TestCase):
    API_ENDPOINT = API_ENDPOINT.format("testuser")
    client = PythonAnywhere("api_key", username="testuser")

    def asserts(self, expected_url, method="GET", data=None):
        assert len(responses.calls) == 1
        call = responses.calls[0]
        assert call.response.url == self.get_url(expected_url)
        assert call.request.method == method
        if data:
            for argument in data:
                assert argument in call.response.request.body

    def get_url(self, url):
        return self.API_ENDPOINT + url
