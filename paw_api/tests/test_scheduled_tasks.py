from .paw_test_suite import PythonAnywhereTestCase
import responses


class TestTasks(PythonAnywhereTestCase):

    @responses.activate
    def test_list_scheduled_tasks(self):
        url_path = "schedule/"
        responses.add(responses.GET, self.get_url(url_path))
        self.client.schedule()
        self.asserts(url_path)

    @responses.activate
    def test_create_scheduled_task(self):
        url_path = "schedule/"
        input_data = {
            "command": "python37 test_script.py",
            "enabled": True,
            "interval": "daily",
            "hour": "12",
            "minute": "00"
        }
        responses.add(responses.POST, self.get_url(url_path))
        self.client.schedule.create(data=input_data)
        expected_data = [
            "command=python37+test_script.py",
            "enabled=True",
            "interval=daily",
            "hour=12",
            "minute=00"
        ]
        self.asserts(url_path, "POST", expected_data)

    @responses.activate
    def test_get_task_by_id(self):
        url_path = "schedule/123/"
        responses.add(responses.GET, self.get_url(url_path))
        self.client.schedule(task_id=123)
        self.asserts(url_path)

    @responses.activate
    def test_update_all_task_fields_by_id(self):
        url_path = "schedule/123/"
        input_data = {
            "command": "python37 test_script.py",
            "enabled": True,
            "interval": "daily",
            "hour": "12",
            "minute": "00"
        }
        responses.add(responses.PUT, self.get_url(url_path))
        self.client.schedule.update(task_id=123, data=input_data)
        expected_data = [
            "command=python37+test_script.py",
            "enabled=True",
            "interval=daily",
            "hour=12",
            "minute=00"
        ]
        self.asserts(url_path, "PUT", expected_data)

    @responses.activate
    def test_update_specific_task_fields_by_id(self):
        url_path = "schedule/123/"
        input_data = {
            "enabled": False,
            "hour": "12",
        }
        responses.add(responses.PATCH, self.get_url(url_path))
        self.client.schedule.update_specific(task_id=123, data=input_data)
        expected_data = [
            "enabled=False",
            "hour=12",
        ]
        self.asserts(url_path, "PATCH", expected_data)

    @responses.activate
    def test_delete_task_by_id(self):
        url_path = "schedule/123/"
        responses.add(responses.DELETE, self.get_url(url_path))
        self.client.schedule.delete(task_id=123)
        self.asserts(url_path, "DELETE")
