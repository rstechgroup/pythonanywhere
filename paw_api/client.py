import getpass
import os
import requests

API_ENDPOINT = "https://www.pythonanywhere.com/api/v0/user/{}/"

# Maps certain function names to HTTP verbs
VERBS = {
    "create": "POST",
    "read": "GET",
    "update": "PUT",
    "update_specific": "PATCH",
    "delete": "DELETE"
}

# Map additional function name to HTTP verbs without removing them from path
ADDITIONAL_VERBS = {
    "send": "POST"
}

# A list of identifiers that should be extracted and placed into the url
# string if they are passed into the kwargs.
IDENTIFIERS = {
    "console_id": "consoles",
    "path": "path",
    "domain_name": "webapps",
    "static_id": "static_files",
    "task_id": "schedule"
}


# Define custom exceptions
class PythonAnywhereError(Exception):

    def __init__(self, message):
        super().__init__(message)


class PythonAnywhere(object):
    """
    A client for the PythonAnywhere API.
    """
    api_key = ""
    url_path = []
    username = None

    def __init__(self, api_key=None, url_path=None, username=None):
        """
        :param api_key: The API key for your PythonAnywhere account.
        :param url_path: The current path constructed for this request.
        :param username: PythonAnywhere username
        """
        self.api_key = api_key or os.environ["API_TOKEN"]
        self.url_path = url_path or []
        self.username = username or getpass.getuser()

    def __getattr__(self, attr):
        """
        Uses attribute chaining to help construct the url path of the request.
        """
        try:
            return object.__getattr__(self, attr)
        except AttributeError:
            return PythonAnywhere(self.api_key, self.url_path + [attr], self.username)

    def construct_request(self, **kwargs):
        """
        :param kwargs: The arguments passed into the request. Valid values are:
            "console_id", "domain_name", and "static_id" will be extracted and
            placed into the url. "data" and "files" will be passed seperately.
            Remaining kwargs will be passed as params into request.
        """
        url_path = self.url_path[:]

        # Find the HTTP method if we were called with create(), update(),
        # read(), or delete()
        if url_path[-1] in VERBS.keys():
            action = url_path.pop()
            method = VERBS[action]
        elif url_path[-1] in ADDITIONAL_VERBS.keys():
            action = url_path.pop()
            method = ADDITIONAL_VERBS[action]
        else:
            method = "GET"

        # Extract certain kwargs and place them in the url instead
        for identifier, name in IDENTIFIERS.items():
            value = kwargs.pop(identifier, None)
            if value:
                url_path.insert(url_path.index(name) + 1, str(value))

        # Need to pass data separately from rest of kwargs
        data = kwargs.pop("data", None)

        # Need to pass files separately from rest of kwargs
        files = kwargs.pop("files", None)

        # Build url
        url = API_ENDPOINT.format(self.username)
        url = url + "/".join(url_path) + "/"

        return url, method, data, files, kwargs

    @staticmethod
    def make_request(url, method, token, **kwargs):
        """
        Actually responsible for making the HTTP request.
        :param url: The URL to load.
        :param method: The HTTP method to use.
        :param token: PythonAnywhere API token found at
            https://www.pythonanywhere.com/user/USERNAME/account/#api_token
        :param kwargs: Values are passed into :class:`Request <Request>`
        """
        response = requests.request(
            method=method,
            url=url,
            headers={"Authorization": "Token {}".format(token)},
            **kwargs
        )

        if not response.ok:
            raise PythonAnywhereError(
                "Calling API url: {} \n{} error: {}".format(url, response.status_code, response.text)
            )

        return response

    def __call__(self, **kwargs):
        url, method, data, files, params = self.construct_request(**kwargs)
        return self.make_request(
            url, method, self.api_key, data=data, files=files, params=params.get('params', None)
        )
