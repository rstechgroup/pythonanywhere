from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


setup(
    name='paw_api',
    version="1.0",
    author='Rajkumar Srinivasan',
    author_email='msrajkumar95@gmail.com',
    description='API client to access all the endpoints provided by PythonAnywhere',
    long_description=readme(),
    long_description_content_type="text/markdown",
    url='https://bitbucket.org/msrajkumar95/pythonanywhere',
    license='MIT',
    classifiers=[
        "Intended Audience :: Developers",
        "Intended Audience :: System Administrators",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.1",
        "Programming Language :: Python :: 3.2",
        "Programming Language :: Python :: 3.3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Topic :: Utilities",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
    keywords='pythonanywhere paw paw-api pythonanywhere_api paw_api',
    packages=['paw_api'],
    install_requires=[
        'requests',
        'markdown',
    ],
    test_suite='nose.collector',
    tests_require=[
        'nose',
        'nose-cover3',
        'responses',
    ],
    include_package_data=True,
    zip_safe=False,
    dependency_links=['https://bitbucket.org/msrajkumar95/pythonanywhere/src/master']
)
